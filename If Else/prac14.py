#14. Write a program to input electricity unit charges and calculate total electricity bill according to the given condition:

unit=int(input('Enter Electricity Unit : '))
ch=0
if(unit<=50):
    ch=unit*0.50
    print(str(ch))
elif(unit>50 and unit<=150):
    ch=25+((unit-50)*0.75)
    print(str(ch))
elif(unit>150 and unit<=250):
    ch=100+((unit-150)*1.20)
    print(str(ch))
else:
    ch=220+((unit-250)*1.50)
    print(ch)
sur=ch*0.20
print('Surcharge : '+str(sur))
print('Total Bill : '+str(ch+sur))