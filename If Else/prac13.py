#13. Write a program to input marks of five subjects Physics, Chemistry, Biology, Mathematics and Computer. Calculate percentage and grade according to following:
Physics=int(input("Enter marks of Physics : "))
Chemistry=int(input("Enter marks of Chemistry : "))
Biology=int(input("Enter marks of Biology : "))
Mathematics=int(input("Enter marks of Mathematics : "))
Computer=int(input("Enter marks of Computer : "))

total=(Physics+Chemistry+Biology+Mathematics+Computer)
print("Total : "+str(total))
percentage=total/500
print("Percentage : "+str(percentage))
if(percentage>=0.9):
    print("Grade A")
elif(percentage>=0.8):
    print("Grade B")
elif(percentage>=0.7):
    print("Grade C")
elif(percentage>=0.6):
    print("Grade D")
elif(percentage>=0.4):
    print("Grade E")
else:
    print("Grade F")