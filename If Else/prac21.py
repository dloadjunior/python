#21. Write a program to accept a coordinate point in a XY coordinate system and determine in which quadrant the coordinate point lies. Hint: Cartesan Point System

x=int(input('Enter x value : '))
y=int(input('Enter y value : '))

if(x>=0 and y>=0):
    print('1st Quadrant')
elif(x>=0 and y<0):
    print('4th Quadrant')
elif(x<0 and y>=0):
    print('2nd Quadrant')
else:
    print('3rd Quadrant')