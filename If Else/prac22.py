#22. Write a single program to convert temperature from Fahrenheit to Celsius and Celsius to Fahrenheit based on what action user chooses.
print('Select an option ')
ch=int(input('1. Fahrenheit to Celsius \n2. Celsius to Fahrenheit : '))
if(ch==1):
    fah=int(input('Enter Fahrenheit : '))
    print('Celsius : '+str((fah-32)*5/9))
elif(ch==2):
    cel=int(input('Enter Celsius : '))
    print('Fahrenheit : '+str((cel*9/5)+32))
else:
    print('Invalid Option')