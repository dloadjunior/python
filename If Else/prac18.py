#18. Write a program to calculate the area of the triangle if all sides are given. Hint: Heron's formula
import math

side1=int(input("Enter Side 1 : "))
side2=int(input("Enter Side 2 : "))
side3=int(input("Enter Side 3 : "))

#semi-perimeter
sp=(side1+side2+side3)/2

s1=sp-side1
s2=sp-side2
s3=sp-side3
area=math.sqrt(sp*s1*s2*s3)
print("Area of Triangle : "+str(area))