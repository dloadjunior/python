#7. Write a program to input any alphabet and check whether it is vowel or consonant. - Hint: ASCII
al=input("Enter an Alphabet : ")
if(al=='a' or al=='e' or al=='i' or al=='o' or al=='u' or al=='A' or al=='E' or al=='I' or al=='O' or al=='U'):
    print(al+" is a Vowel")
else:
    print(al+" is not a Vowel")