-1. Write a program to find maximum between two numbers.
-2. Write a program to find maximum between three numbers.
-3. Write a program to check whether a number is negative, positive or zero.
-4. Write a program to check whether a number is divisible by 5 and 11 or not.
-5. Write a program to check whether a number is even or odd - Try only if you know whats mod op
-6. Write a program to check whether a year is leap year or not.
-7. Write a program to input any alphabet and check whether it is vowel or consonant. - Hint: ASCII
-8. Write a program to check whether a character is uppercase or lowercase alphabet.- Hint: ASCII
-9. Write a program to input angles of a triangle and check whether triangle is valid or not.
-10. Write a program to check whether the triangle is equilateral, isosceles or scalene triangle.
-11. Write a program to find all roots of a quadratic equation.
-12. Write a program to calculate profit or loss. - Take Cost Price and Sell Price from the user and detemine if the transaction is a Profit or a loss
-13. Write a program to input marks of five subjects Physics, Chemistry, Biology, Mathematics and Computer. Calculate percentage and grade according to following:
Percentage >= 90% : Grade A
Percentage >= 80% : Grade B
Percentage >= 70% : Grade C
Percentage >= 60% : Grade D
Percentage >= 40% : Grade E
Percentage < 40% : Grade F

14. Write a program to input electricity unit charges and calculate total electricity bill according to the given condition:
For first 50 units Rs. 0.50/unit
For next 100 units Rs. 0.75/unit
For next 100 units Rs. 1.20/unit
For unit above 250 Rs. 1.50/unit

An additional surcharge of 20% is added to the bill

15. Write a program to convert days into years, weeks and days.
-16. Write a program to find power of any number x ^ y.
-17. Write a program to enter base and height of a triangle and find its area.
-18. Write a program to calculate the area of the triangle if all sides are given. Hint: Heron's formula
-19. Write a program to enter P, T, R and calculate Compound Interest.
20. Write a program to calculate the BMI of the person
-21. Write a program to accept a coordinate point in a XY coordinate system and determine in which quadrant the coordinate point lies. Hint: Cartesan Point System
22. Write a single program to convert temperature from Fahrenheit to Celsius and Celsius to Fahrenheit based on what action user chooses.
-23. Write a program to take values of a and b to find (a + b)^2. Use algebra formula
-24. Write a program to calculate the volume of the sphere, total surface area of a cuboid, total surface area of the cynlinder
-25. Write a program that asks the user for their name and greets them with their name.
-26. Modify the previous program such that only the users Alice and Bob are greeted with their names.
-27. Write a program that asks the user for a number n and prints the sum of the numbers 1 to n
-28. Take Name, Date of Birth, Gender and Percentage of 5 students. Print the entered data sequentially such that it looks like a table
-29. Write a program to take 2 integers from the user and check if they're both in the range 40-50 inclusive or they're both in the range 50-60 inclusive or each of them is in one of the ranges or none of them are a part of any range
-30. Attempt only after solving #5: Write a program to take two numbers from the user and check if their last digits is same.
31. Write a program to print a block F using hash (#), where the F has a height of six characters and width of five and four characters. And also to print a big 'C'.
-32. Write a program to calculate the distance between the two points. Your inputs will have a pair of x1, y1 and x2, y2.
33. Write a program to print the roots of Bhaskara’s formula from the given three floating numbers. Display a message if it is not possible to find the roots.
34. Write a program to print the result of the following operations. I want you to solve these on paper first.
Test Data:
a. -5 + 8 * 6
b. (55+9) % 9
c. 20 + -3*5 / 8
d. 5 + 15 / 3 * 2 - 8 % 3

35. Write a program to print the American flag on the screen.